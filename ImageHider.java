
/**
 * Hides images in text files and retrieves them.
 * <p>
 * Takes an image file, converts to base64, encrypts with AES, and stores in the :obf
 * alternate data stream (ADS) of chosen *.txt file (or new *.txt file).
 * Takes a text file with appended :obf ADS, decrypts AES, converts base64 to raw, and 
 * writes data to an image file.
 * 
 * NOTE: This program will only work within the NTFS filesystem!
 *
 * @author <a href="mailto:brandon.k.garner@gmail.com">Brandon Garner</a>
 * date: December 3, 2017
 */
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class ImageHider {

	// Read bytes from normal image file and convert to base64 String
	public static String pictureToString64(String imagePath) throws FileNotFoundException, IOException {
		// Read normal image from file system
		File file = new File(imagePath);
		FileInputStream imageInFile = new FileInputStream(file);
		byte imageData[] = new byte[(int) file.length()];
		imageInFile.read(imageData);
		imageInFile.close();

		// Convert image to base64-encoded String and return that String
		return Base64.getEncoder().encodeToString(imageData);
	}

	// Take base64 String, convert to byte array, and Write bytes to normal image
	// file
	public static void string64ToPicture(String image64, String pathFile) throws FileNotFoundException, IOException {
		FileOutputStream imageOutFile = new FileOutputStream(pathFile);
		// Decode base64 String back to byte data
		byte[] imageByteArray = Base64.getDecoder().decode(image64);
		// Write decoded data to (image) file
		imageOutFile.write(imageByteArray);
		imageOutFile.close();
	}

	// Write raw and unpadded data to ADS
	private static void writeADS(String hiddenData, String textFile) throws IOException {
		// If file already exists, delete Alternate Data Streams
		// Refer to clearADS() comments for more info
		/*
		 * File look = new File(textFile); if(look.exists() && !look.isDirectory()) {
		 * clearADS(textFile); }
		 */
		// Write to ADS
		File file = new File(textFile + ":obf");
		PrintWriter adsOutStream = new PrintWriter(new FileOutputStream(file, true));
		adsOutStream.print(hiddenData);
		adsOutStream.close();
	}

	// Read raw and unpadded data from ADS
	private static String readADS(String textFile) throws IOException {
		String dataFromADS = "";
		int n = 0;

		// Read each char from file one-by-one, cast into char, and append to String
		FileInputStream adsInStream = new FileInputStream(textFile + ":obf");
		while ((n = adsInStream.read()) != -1) {
			dataFromADS += ((char) n);
		}
		adsInStream.close();

		// Return ADS [encrypted] data as a String
		return dataFromADS;
	}

	public static void hidePicture(String textFile, String pathToPicture, char[] key) {
		try {
			writeADS(AES.encrypt(pictureToString64(pathToPicture), key), textFile);
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException ioe) {
			System.out.println("Exception occured while reading image: " + ioe);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static void retrievePicture(String textFile, String pathToOutputPicture, char[] key) throws Throwable {
		string64ToPicture(AES.decrypt(readADS(textFile), key), pathToOutputPicture);
	}

	// *PictureGUI Methods Throw Exceptions to be handled graphically
	public static void hidePictureGUI(String textFile, String pathToPicture, char[] key) throws Throwable {
		writeADS(AES.encrypt(pictureToString64(pathToPicture), key), textFile);
	}

	public static void retrievePictureGUI(String textFile, String pathToOutputPicture, char[] key) throws Throwable {
		string64ToPicture(AES.decrypt(readADS(textFile), key), pathToOutputPicture);
	}

	// System commands to remove all Alternate Data Streams
	// Causing strange issues though the code should work theoretically
	// Easy workaround-- delete the file if it already contains ADS:obf and remake new file
	/*
	private static void clearADS(String textFile) {

		String textFileWinAbsolute = textFile.replace("/", "\\");
		textFileWinAbsolute = System.getenv("SystemDrive").toString() + textFileWinAbsolute;

		try {
			// Cat into new text file
			String command1 = "cmd /c type " + textFileWinAbsolute + " > " + getPWD() + "\\tempClearADS.txt";
			Runtime.getRuntime().exec(command1);
			// Delete original
			String command2 = "cmd /c delete " + textFileWinAbsolute;
			Runtime.getRuntime().exec(command2);
			// Remake file with new data (causing streams to be deleted)
			String command3 = "cmd /c type " + getPWD() + "\\tempClearADS.txt > " + textFileWinAbsolute;
			Runtime.getRuntime().exec(command3);
			// Cleanup temp files
			String command4 = "cmd /c delete " + getPWD() + "\\tempClearADS.txt";
			Runtime.getRuntime().exec(command4);
			
		 	// For testing purposes 
		 	// System.out.println(command1);
			// System.out.println(command2); 
			// System.out.println(command3);
			// System.out.println(command4);
			 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	*/
	
	public static String getPWD() {
		return System.getProperty("user.dir");
	}

	public static void test() {
		char[] testPass = { 1, 2, 3, 4, 5 };
		System.out.print("********************Embedding photo********************: ");
		hidePicture(getPWD() + "\\test.txt", getPWD() + "\\test.jpg", testPass);
		System.out.println("DONE");

		System.out.print("********************Retreiving photo*******************: ");
		try {
			retrievePicture(getPWD() + "\\test.txt", getPWD() + "\\testOutput.jpg", testPass);
		} catch (BadPaddingException e) {
			// This block cannot be reached since same variable value given for
			// encryption/decryption password
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("DONE");
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean exit = false;
		System.out.println("\n 33Welcome to Image Hider! \n");
		while (!exit) {
			System.out.print("  1) Hide image in text file \n" + "  2) Retreive image from text file \n"
					+ "  3) Run a test \n" + "  4) Quit \n" + "Select an option: "

			);
			int selection = input.nextInt();
			input.nextLine();

			switch (selection) {
			case 1:
				System.out.println("PATHS MUST USE UNIX CONVENTION OR DOUBLE BACKSLASHES");
				System.out.println("EXAMPLE: /Users/username/Pictures/test.jpg  or "
						+ "C:\\\\Users\\\\username\\\\Pictures\\\\test.jpg \n");

				Scanner userData = new Scanner(System.in);
				System.out.println("Enter path to your image file that will be hidden: ");
				String pathToPicture = userData.nextLine();
				System.out.println("Enter path to your text file: ");
				String textFile = userData.nextLine();

				// char[] key=userData.next().toCharArray(); // Less code, but also less secure
				// Taken as array so entire password isn't stored in memory
				Console console = System.console();
				char[] key;
				char[] keyConf;
				do {
					System.out.println("Enter a password for encryption: ");
					key = console.readPassword();
					System.out.println("Confirm password for encryption: ");
					keyConf = console.readPassword();
					console.flush();
				} while (!Arrays.equals(key, keyConf));

				hidePicture(textFile, pathToPicture, key);
				System.out.println("DONE! \n");
				break;
			case 2:
				System.out.println("PATHS MUST USE UNIX CONVENTION OR DOUBLE BACKSLASHES");
				System.out.println("EXAMPLE: /Users/username/Pictures/test.jpg  or "
						+ "C:\\\\Users\\\\username\\\\Pictures\\\\test.jpg \n");

				Scanner userData2 = new Scanner(System.in);
				System.out.println("Enter path to your text file with a hidden image: ");
				String textFile2 = userData2.nextLine();
				System.out.println("Enter path to output your image file: ");
				String pathToOutputPicture = userData2.nextLine();

				// char[] key=userData2.next().toCharArray(); // Less code, but also less secure
				// Taken as array so entire password isn't stored in memory
				boolean success = false;
				int tooManyTries = 0;
				Console console2 = System.console();
				char[] key2;
				// Get decryption password; maximum of 3 tries
				do {
					console2.flush();
					System.out.println("Enter the password for decryption: ");
					key2 = console2.readPassword();
					try {
						retrievePicture(textFile2, pathToOutputPicture, key2);
						success = true;
					} catch (BadPaddingException e) {
						System.out.println("Incorrect Password!");
					} catch (Throwable e) {
						e.printStackTrace();
					}
					tooManyTries++;
				} while (tooManyTries != 3 && !success);
				if (tooManyTries == 3)
					System.out.println("Maximum number of password attempts exceeded! \n");
				else
					System.out.println("DONE! \n");
				break;
			case 3:
				File f = new File(getPWD() + "\\test.jpg");
				if (f.exists() && !f.isDirectory()) {
					test();
					System.out.println("DONE! \n");
				} else
					System.out.println(
							"To run a test an image called test.jpg is required in the same location this program is running from. \n");
				break;
			case 4:
				exit = true;
				break;
			default:
				System.out.println("Invalid choice.");
				break;
			}
		}
		input.close();
		System.out.println("Exiting...");
	}
}

class AES {
	private static SecretKeySpec secretKey;
	private static byte[] key;

	// Creates key from char array
	private static void setKey(char[] myKey) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// Using char[] so password will not be stored in memory
		// char[] -> byte[]
		key = new byte[myKey.length << 1];
		for (int i = 0; i < myKey.length; i++) {
			int bpos = i << 1;
			key[bpos] = (byte) ((myKey[i] & 0xFF00) >> 8);
			key[bpos + 1] = (byte) (myKey[i] & 0x00FF);
		}

		MessageDigest sha = null;
		// key = myKey.getBytes("UTF-8");
		sha = MessageDigest.getInstance("SHA-1");
		key = sha.digest(key);
		key = Arrays.copyOf(key, 16);
		secretKey = new SecretKeySpec(key, "AES");
	}

	// Encrypts String using generated key from char array
	public static String encrypt(String strToEncrypt, char[] secret) throws Throwable {
		setKey(secret);
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);

		// Base64 ("UTF-8") is needed again to ensure integrity of data; Errors
		// encountered when Base64 only called prior to encrypt() method
		// .withoutPadding() removes '==' that reader cannot understand
		return Base64.getEncoder().withoutPadding().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
	}

	// Decrypts String using key that originally generated String
	public static String decrypt(String strToDecrypt, char[] secret) throws Throwable {
		setKey(secret);
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);

		// Base64 ("UTF-8") is needed again to ensure integrity of data; Errors
		// encountered when Base64 only called prior to encrypt() method
		return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
	}
}