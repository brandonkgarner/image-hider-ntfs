/**
 * Hides images in text files and retrieves them. (GUI COMPONENT)
 * <p>
 * Takes an image file, converts to base64, encrypts with AES, and stores in the :obf
 * alternate data stream (ADS) of chosen *.txt file (or new *.txt file).
 * Takes a text file with appended :obf ADS, decrypts AES, converts base64 to raw, and 
 * writes data to an image file.
 * 
 * NOTE: This program will only work within the NTFS filesystem!
 *
 * @author <a href="mailto:brandon.k.garner@gmail.com">Brandon Garner</a>
 * date: December 3, 2017
 */
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import javax.crypto.BadPaddingException;
import javax.swing.BoxLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

public class ImageHiderGUI {

	private JFrame frmImageHider;
	private JTextField formHideImagePath;
	private JTextField formHideTextPath;
	private JPasswordField formHidePassword;
	private JPasswordField formHidePasswordConf;
	
	private JTextField formRetTextPath;
	private JTextField formRetImageOutPath;
	private JPasswordField formRetPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImageHiderGUI window = new ImageHiderGUI();
					window.frmImageHider.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ImageHiderGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmImageHider = new JFrame();
		frmImageHider.setTitle("Image Hider");
		frmImageHider.setBounds(100, 100, 768, 330);

		frmImageHider.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmImageHider.getContentPane().setLayout(new BoxLayout(frmImageHider.getContentPane(), BoxLayout.X_AXIS));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmImageHider.getContentPane().add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Hide Image in Text File", null, panel, null);

		JLabel lblTextFileTo = new JLabel("Text file to hide inside:");

		formHideTextPath = new JTextField();
		formHideTextPath.setColumns(10);

		JButton btnNewButton = new JButton("Choose File");
		panel.setLayout(
				new MigLayout("", "[329px][35px][329px][461px,grow][118px][]", "[][25px][][16px][25px][][][][][][][][22px][48px]"));

		JLabel lblPictureToBe = new JLabel("Picture to be hidden:");
		panel.add(lblPictureToBe, "cell 0 0,alignx left,aligny bottom");

		formHideImagePath = new JTextField();
		formHideImagePath.setColumns(10);
		panel.add(formHideImagePath, "cell 0 1 4 1,growx,aligny center");

		JButton btnChooseFile = new JButton("Choose File");
		panel.add(btnChooseFile, "cell 4 1 2 1,growx,aligny top");
		btnChooseFile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					// System.out.println(selectedFile.getName());
					formHideImagePath.setText(selectedFile.getAbsolutePath());
				}

			}
		});
		panel.add(lblTextFileTo, "cell 0 3,alignx left,aligny bottom");
		panel.add(formHideTextPath, "cell 0 4 4 1,growx,aligny center");
		panel.add(btnNewButton, "cell 4 4 2 1,growx,aligny top");

		JLabel lblAesEncryptionPassword = new JLabel("AES encryption password:");
		panel.add(lblAesEncryptionPassword, "cell 0 6,alignx left,aligny bottom");
		
		JLabel lblConfirmPassword = new JLabel("Confirm password:");
		panel.add(lblConfirmPassword, "cell 2 6");

		formHidePassword = new JPasswordField();
		formHidePassword.setColumns(10);
		panel.add(formHidePassword, "cell 0 7,growx,aligny center");
		
		formHidePasswordConf = new JPasswordField();
		formHidePasswordConf.setHorizontalAlignment(SwingConstants.LEFT);
		formHidePasswordConf.setColumns(10);
		panel.add(formHidePasswordConf, "cell 2 7,growx,aligny center");

		// Submit form for hiding when button is clicked and all fields are filled out
		JButton btnNewButton_1 = new JButton("Hide Picture!");
		panel.add(btnNewButton_1, "cell 0 13 5 1,grow");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Check for blank fields on submission
				if (formHideImagePath.getText().equals("") || formHideTextPath.getText().equals("")
						|| formHidePassword.getPassword().length == 0 || formHidePasswordConf.getPassword().length == 0) {
					infoBox("All fields must be filled!", "Error: Missing Options");
				} else if (!Arrays.equals(formHidePassword.getPassword(), formHidePasswordConf.getPassword())) {
					infoBox("Passwords do not match!", "Error: Password");
				} else {
					try {
						// Call method to handle hiding of picture
						ImageHider.hidePictureGUI(formHideTextPath.getText(), formHideImagePath.getText(), formHidePassword.getPassword());
						infoBox("Your image has been embedded to " + formHideTextPath.getText(), "Sucess: Process Complete");
						// Clear form fields
						formHideImagePath.setText(null);
						formHideTextPath.setText(null);
						formHidePassword.setText(null);
						formHidePasswordConf.setText(null);
					} catch (FileNotFoundException e1) {
						infoBox("Please check your filepaths!", "Error: File Not Found");
					} catch (UnsupportedEncodingException e1) {
						infoBox("Something went wrong when encrypting your file!", "Error: Unsupported Encoding Type");
					} catch (IOException e1) {
						infoBox("Something went wrong when writing your file!", "Error: IOException");
					} catch (NoSuchAlgorithmException e1) {
						infoBox("Something went wrong when encrypting your file!", "Error: No Such Algorithm");
					} catch (Throwable e1) {
						infoBox("An unknown error has occured; please try again!", "Error");
					}
				}
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					// System.out.println(selectedFile.getName());
					formHideTextPath.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Recover Image from Text File", null, panel_1, null);
		panel_1.setLayout(new MigLayout("", "[329px][35px][461px][118px][]", "[][][][][][][][][][][][25px][48px]"));

		JButton button_1 = new JButton("Choose File");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					// System.out.println(selectedFile.getName());
					formRetTextPath.setText(selectedFile.getAbsolutePath());
				}
			}
		});

		JLabel lblTextFileWith = new JLabel("Text file with hidden image:");
		panel_1.add(lblTextFileWith, "cell 0 0,alignx left,aligny bottom");

		formRetTextPath = new JTextField();
		formRetTextPath.setColumns(10);
		panel_1.add(formRetTextPath, "cell 0 1 3 1,growx,aligny center");
		panel_1.add(button_1, "cell 3 1 2 1,growx,aligny top");

		JLabel lblOutputPathFor = new JLabel("Output path for image: ");
		panel_1.add(lblOutputPathFor, "cell 0 3,alignx left,aligny bottom");

		// Submit form for recovery when button is clicked and all fields are filled out
		JButton btnRecoverPicture = new JButton("Recover Picture!");
		btnRecoverPicture.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// Check for blank fields on submission
				if (formRetTextPath.getText().equals("") || formRetImageOutPath.getText().equals("")
						|| formRetPassword.getPassword().length == 0) {
					infoBox("All fields must be filled!", "Error: Missing Options");
				} else {
					try {
						// Call method to handle retrieval of picture
						ImageHider.retrievePictureGUI(formRetTextPath.getText(), formRetImageOutPath.getText(),
								formRetPassword.getPassword());
						infoBox("Your image has been written to " + formRetImageOutPath.getText(), "Sucess: Process Complete");
						// Clear form fields
						formRetTextPath.setText(null);
						formRetImageOutPath.setText(null);
						formRetPassword.setText(null);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						infoBox("Please check your filepaths!", "Error: File Not Found");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						infoBox("Something went wrong when writing your file!", "Error: IOException");
					} catch (BadPaddingException e1) {
						// TODO Auto-generated catch block
						infoBox("Your decryption key is incorrect; please ensure you have the correct password!",
								"Error: Incorrect Password");
					} catch (Throwable e1) {
						// TODO Auto-generated catch block
						infoBox("An unknown error has occurred during decryption; please try again!",
								"Error: Decryption Error");
					}
				}
			}
		});

		formRetImageOutPath = new JTextField();
		formRetImageOutPath.setColumns(10);
		panel_1.add(formRetImageOutPath, "cell 0 4 3 1,growx,aligny center");

		JButton button_2 = new JButton("Choose File");
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				int returnValue = fileChooser.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					// System.out.println(selectedFile.getName());
					formRetImageOutPath.setText(selectedFile.getAbsolutePath());
				}
			}
		});
		panel_1.add(button_2, "cell 3 4 2 1,growx,aligny top");

		JLabel label_1 = new JLabel("AES encryption password:");
		panel_1.add(label_1, "cell 0 6,alignx left,aligny bottom");

		formRetPassword = new JPasswordField();
		formRetPassword.setColumns(10);
		panel_1.add(formRetPassword, "cell 0 7,growx,aligny center");
		panel_1.add(btnRecoverPicture, "cell 0 12 4 1,grow");
	}

	public static void infoBox(String infoMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
	}
}

